<?php

/*
 * Title:       SrPago Payment Gateway
 * Author:      Roberto Ramírez
 * URL:         https://www.srpago.com
 */

namespace Srpago\SrpagoPayments\Model\Source;

class CashStores
{
    /**
     * Regresa la lista de los lugares donde se puede pagar.
     *
     * @return array
     */
    public static function toArray()
    {
        return [
            'OXXO' => 'OXXO',
            // 'SEVEN_ELEVEN' => '7-Eleven',
            // 'EXTRA' => 'Extra',
            // 'ELEKTRA' => 'Elektra',
            // 'COPPEL' => 'Coppel',
            // 'FARMACIA_BENAVIDES' => 'Farmacias Benavides',
            // 'FARMACIA_ESQUIVAR' => 'Farmacias Esquivar',
            // 'BANAMEX' => 'Citibanamex',
            // 'BANORTE' => 'Banorte',
            // 'INBURSA' => 'Inbursa',
            // 'SCOTIABANK' => 'Scotiabank',
        ];
    }
}
