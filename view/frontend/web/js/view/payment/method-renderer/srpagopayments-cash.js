/*browser:true*/
/*global define*/
define([
    'jquery',
    'mage/storage',
    'Magento_Checkout/js/view/payment/default',
    'Magento_Checkout/js/model/quote'
], function ($, storage, Component, quote) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Srpago_SrpagoPayments/payment/cash-form'
        },

        initialize : function() {
            this._super();
        },

        getCode: function () {
            return 'srpagopayments_cash';
        },

        isActive: function () {
            return true;
        },

        getInstructions: function () {
            return window.checkoutConfig.payment.srpago.cash_form.instructions;
        },

        getStores: function () {
            return window.checkoutConfig.payment.srpago.cash_form.stores;
        },

        getStoresValues: function () {
            return _.map(this.getStores(), function (key, value) {
                return {
                    'label': key,
                    'value': value,
                }
            });
        },

        getData: function() {
            return {
                'method': this.item.method,
                'additional_data': {
                    'store': $('#srpago_store').val()
                }
            };
        },
    });
});
