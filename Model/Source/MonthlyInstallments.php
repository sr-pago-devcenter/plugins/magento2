<?php

/*
 * Title:       SrPago Payment Gateway
 * Author:      Roberto Ramírez
 * URL:         https://www.srpago.com
 */

namespace Srpago\SrpagoPayments\Model\Source;

use Magento\Framework\Option\ArrayInterface;

class MonthlyInstallments implements ArrayInterface
{
    /**
     * Meses soportados para pagos diferidos.
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 3,
                'label' => __('3 Meses'),
            ],
            [
                'value' => 6,
                'label' => __('6 Meses'),
            ],
            [
                'value' => 9,
                'label' => __('9 Meses'),
            ],
            [
                'value' => 12,
                'label' => __('12 Meses'),
            ],
        ];
    }

    /**
     * Obtiene los meses soportados en formato "key-value".
     *
     * @return array
     */
    public function toArray()
    {
        return [
            3   => __('3 Meses'),
            6   => __('6 Meses'),
            9   => __('9 Meses'),
            12  => __('12 Meses'),
        ];
    }
}
