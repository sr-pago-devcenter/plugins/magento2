<?php

/**
 * Sr. Pago (https://srpago.com)
 *
 * @link      https://api.srpago.com
 * @copyright Copyright (c) 2016 SR PAGO
 * @license   http://opensource.org/licenses/BSD-2-Clause BSD-2-Clause
 * @package   SrPago
 */

namespace SrPago;

/**
 * Class Stores
 *
 * @package SrPago
 */
class Stores extends Base
{
    const ENDPOINT = '/payment/convenience-store';

    /**
     * @param array $data
     * @return mixed
     */
    public function create($data)
    {
        $response = $this->httpClient()->post(static::ENDPOINT, $data);

        return $response;
    }
}
