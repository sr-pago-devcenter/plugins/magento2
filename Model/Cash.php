<?php

/**
 * Title:       SrPago Payment Gateway
 * Author:      Roberto Ramírez
 * URL:         https://www.srpago.com
 */

namespace Srpago\SrpagoPayments\Model;

use Magento\Framework\Phrase;
use Magento\Payment\Model\InfoInterface;
use Magento\Sales\Model\Order;
use SrPago\Error\SrPagoError;
use SrPago\Http\HttpClient;
use SrPago\SrPago;
use SrPago\SrpagoPayments\Model\Config;
use Srpago\SrpagoPayments\Model\Source\CashStores;
use Srpago\SrpagoPayments\Util\SrPagoMisc;
use Magento\Framework\Validator\Exception as ValidatorException;

class Cash extends Offline
{
    /**
     * Código del metodo de pago.
     *
     * @var string
     */
    const CODE = 'srpagopayments_cash';

    protected $_code = self::CODE;

    public function createPaymentOrder($payment, $store)
    {
        if (! isset(CashStores::toArray()[$store])) {
            throw new ValidatorException(
                new Phrase(__('La sucursal de pago no es válida.'))
            );
        }

        Config::initializeSrPagoLibrary($this->isSandbox, $this->appKey, $this->appSecret);

        $order = $payment->getOrder();
        $amount = $order->getBaseGrandTotal();

        try {
            $SrPagoCharge = SrPago::Stores();

            $response = $SrPagoCharge->create([
                'payment' => [
                    'reference' => [
                        'number' => $order->getIncrementId(),
                        'description' => sprintf('#%s, %s', $order->getIncrementId(), $order->getCustomerEmail()),
                    ],
                ],
                'total' => $amount,
                'store' => $store,
                'email' => $order->getCustomerEmail(),
            ]);

            return SrPagoMisc::parseResponse($response, HttpClient::$last);
        } catch(SrPagoError $e) {
            $error = $e->getError();

            switch ($error['code']) {
                case 'InvalidParamException':
                case 'InvalidEncryptionException':
                case 'PaymentFilterException':
                case 'SwitchException':
                case 'InternalErrorException':
                default:
                    $error = new Phrase('Ha ocurrido un error al procesar la transacción, favor de volverlo a intentar mas tarde.');
                break;
                case 'SuspectedFraudException':
                case 'InvalidTransactionException':
                case 'PaymentException':
                    $error = new Phrase('La transacción ha sido denegada por el banco emisor.');
                break;
            }

            throw new ValidatorException($error);
        } catch(\Exception $e) {
            throw new ValidatorException(new Phrase($e->getMessage()));
        }
    }

    public function getDefaultStatus()
    {
        return $this->getConfigData('order_status');
    }
}
