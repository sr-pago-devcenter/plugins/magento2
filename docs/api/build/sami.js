
window.projectVersion = 'master';

(function(root) {

    var bhIndex = null;
    var rootPath = '';
    var treeHtml = '        <ul>                <li data-name="namespace:Srpago" class="opened">                    <div style="padding-left:0px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Srpago.html">Srpago</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:Srpago_SrpagoPayments" >                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Srpago/SrpagoPayments.html">SrpagoPayments</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:Srpago_SrpagoPayments_Model" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Srpago/SrpagoPayments/Model.html">Model</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:Srpago_SrpagoPayments_Model_Source" >                    <div style="padding-left:54px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Srpago/SrpagoPayments/Model/Source.html">Source</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Srpago_SrpagoPayments_Model_Source_Cctype" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="Srpago/SrpagoPayments/Model/Source/Cctype.html">Cctype</a>                    </div>                </li>                            <li data-name="class:Srpago_SrpagoPayments_Model_Source_MonthlyInstallments" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="Srpago/SrpagoPayments/Model/Source/MonthlyInstallments.html">MonthlyInstallments</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:Srpago_SrpagoPayments_Model_Ui" >                    <div style="padding-left:54px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Srpago/SrpagoPayments/Model/Ui.html">Ui</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Srpago_SrpagoPayments_Model_Ui_ConfigProvider" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="Srpago/SrpagoPayments/Model/Ui/ConfigProvider.html">ConfigProvider</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:Srpago_SrpagoPayments_Model_Config" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Srpago/SrpagoPayments/Model/Config.html">Config</a>                    </div>                </li>                </ul></div>                </li>                </ul></div>                </li>                </ul></div>                </li>                </ul>';

    var searchTypeClasses = {
        'Namespace': 'label-default',
        'Class': 'label-info',
        'Interface': 'label-primary',
        'Trait': 'label-success',
        'Method': 'label-danger',
        '_': 'label-warning'
    };

    var searchIndex = [
                    
            {"type": "Namespace", "link": "Srpago.html", "name": "Srpago", "doc": "Namespace Srpago"},{"type": "Namespace", "link": "Srpago/SrpagoPayments.html", "name": "Srpago\\SrpagoPayments", "doc": "Namespace Srpago\\SrpagoPayments"},{"type": "Namespace", "link": "Srpago/SrpagoPayments/Model.html", "name": "Srpago\\SrpagoPayments\\Model", "doc": "Namespace Srpago\\SrpagoPayments\\Model"},{"type": "Namespace", "link": "Srpago/SrpagoPayments/Model/Source.html", "name": "Srpago\\SrpagoPayments\\Model\\Source", "doc": "Namespace Srpago\\SrpagoPayments\\Model\\Source"},{"type": "Namespace", "link": "Srpago/SrpagoPayments/Model/Ui.html", "name": "Srpago\\SrpagoPayments\\Model\\Ui", "doc": "Namespace Srpago\\SrpagoPayments\\Model\\Ui"},
            
            {"type": "Class", "fromName": "Srpago\\SrpagoPayments\\Model", "fromLink": "Srpago/SrpagoPayments/Model.html", "link": "Srpago/SrpagoPayments/Model/Config.html", "name": "Srpago\\SrpagoPayments\\Model\\Config", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Srpago\\SrpagoPayments\\Model\\Config", "fromLink": "Srpago/SrpagoPayments/Model/Config.html", "link": "Srpago/SrpagoPayments/Model/Config.html#method_initializeSrPagoLibrary", "name": "Srpago\\SrpagoPayments\\Model\\Config::initializeSrPagoLibrary", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Srpago\\SrpagoPayments\\Model\\Config", "fromLink": "Srpago/SrpagoPayments/Model/Config.html", "link": "Srpago/SrpagoPayments/Model/Config.html#method_getBillingInfo", "name": "Srpago\\SrpagoPayments\\Model\\Config::getBillingInfo", "doc": "&quot;Regresa la informaci\u00f3n de facturaci\u00f3n de la orden.&quot;"},
                    {"type": "Method", "fromName": "Srpago\\SrpagoPayments\\Model\\Config", "fromLink": "Srpago/SrpagoPayments/Model/Config.html", "link": "Srpago/SrpagoPayments/Model/Config.html#method_getDiscountInfo", "name": "Srpago\\SrpagoPayments\\Model\\Config::getDiscountInfo", "doc": "&quot;Regresa la informaci\u00f3n de los descuentos de la orden.&quot;"},
                    {"type": "Method", "fromName": "Srpago\\SrpagoPayments\\Model\\Config", "fromLink": "Srpago/SrpagoPayments/Model/Config.html", "link": "Srpago/SrpagoPayments/Model/Config.html#method_getCustomerInfo", "name": "Srpago\\SrpagoPayments\\Model\\Config::getCustomerInfo", "doc": "&quot;Regresa la informaci\u00f3n del cliente.&quot;"},
                    {"type": "Method", "fromName": "Srpago\\SrpagoPayments\\Model\\Config", "fromLink": "Srpago/SrpagoPayments/Model/Config.html", "link": "Srpago/SrpagoPayments/Model/Config.html#method_getShippingInfo", "name": "Srpago\\SrpagoPayments\\Model\\Config::getShippingInfo", "doc": "&quot;Regresa la informaci\u00f3n de envio de la orden.&quot;"},
                    {"type": "Method", "fromName": "Srpago\\SrpagoPayments\\Model\\Config", "fromLink": "Srpago/SrpagoPayments/Model/Config.html", "link": "Srpago/SrpagoPayments/Model/Config.html#method_getOrderItemsInfo", "name": "Srpago\\SrpagoPayments\\Model\\Config::getOrderItemsInfo", "doc": "&quot;Regresa los productos de la orden.&quot;"},
            
            {"type": "Class", "fromName": "Srpago\\SrpagoPayments\\Model\\Source", "fromLink": "Srpago/SrpagoPayments/Model/Source.html", "link": "Srpago/SrpagoPayments/Model/Source/Cctype.html", "name": "Srpago\\SrpagoPayments\\Model\\Source\\Cctype", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Srpago\\SrpagoPayments\\Model\\Source\\Cctype", "fromLink": "Srpago/SrpagoPayments/Model/Source/Cctype.html", "link": "Srpago/SrpagoPayments/Model/Source/Cctype.html#method_getAllowedTypes", "name": "Srpago\\SrpagoPayments\\Model\\Source\\Cctype::getAllowedTypes", "doc": "&quot;Tipos de tarjetas que son soportadas.&quot;"},
            
            {"type": "Class", "fromName": "Srpago\\SrpagoPayments\\Model\\Source", "fromLink": "Srpago/SrpagoPayments/Model/Source.html", "link": "Srpago/SrpagoPayments/Model/Source/MonthlyInstallments.html", "name": "Srpago\\SrpagoPayments\\Model\\Source\\MonthlyInstallments", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Srpago\\SrpagoPayments\\Model\\Source\\MonthlyInstallments", "fromLink": "Srpago/SrpagoPayments/Model/Source/MonthlyInstallments.html", "link": "Srpago/SrpagoPayments/Model/Source/MonthlyInstallments.html#method_toOptionArray", "name": "Srpago\\SrpagoPayments\\Model\\Source\\MonthlyInstallments::toOptionArray", "doc": "&quot;Meses soportados para pagos diferidos.&quot;"},
                    {"type": "Method", "fromName": "Srpago\\SrpagoPayments\\Model\\Source\\MonthlyInstallments", "fromLink": "Srpago/SrpagoPayments/Model/Source/MonthlyInstallments.html", "link": "Srpago/SrpagoPayments/Model/Source/MonthlyInstallments.html#method_toArray", "name": "Srpago\\SrpagoPayments\\Model\\Source\\MonthlyInstallments::toArray", "doc": "&quot;Obtiene los meses soportados en formato \&quot;key-value\&quot;.&quot;"},
            
            {"type": "Class", "fromName": "Srpago\\SrpagoPayments\\Model\\Ui", "fromLink": "Srpago/SrpagoPayments/Model/Ui.html", "link": "Srpago/SrpagoPayments/Model/Ui/ConfigProvider.html", "name": "Srpago\\SrpagoPayments\\Model\\Ui\\ConfigProvider", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Srpago\\SrpagoPayments\\Model\\Ui\\ConfigProvider", "fromLink": "Srpago/SrpagoPayments/Model/Ui/ConfigProvider.html", "link": "Srpago/SrpagoPayments/Model/Ui/ConfigProvider.html#method___construct", "name": "Srpago\\SrpagoPayments\\Model\\Ui\\ConfigProvider::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Srpago\\SrpagoPayments\\Model\\Ui\\ConfigProvider", "fromLink": "Srpago/SrpagoPayments/Model/Ui/ConfigProvider.html", "link": "Srpago/SrpagoPayments/Model/Ui/ConfigProvider.html#method_getConfig", "name": "Srpago\\SrpagoPayments\\Model\\Ui\\ConfigProvider::getConfig", "doc": "&quot;Regresa la configuraci\u00f3n necesaria para la vista.&quot;"},
            
            
                                        // Fix trailing commas in the index
        {}
    ];

    /** Tokenizes strings by namespaces and functions */
    function tokenizer(term) {
        if (!term) {
            return [];
        }

        var tokens = [term];
        var meth = term.indexOf('::');

        // Split tokens into methods if "::" is found.
        if (meth > -1) {
            tokens.push(term.substr(meth + 2));
            term = term.substr(0, meth - 2);
        }

        // Split by namespace or fake namespace.
        if (term.indexOf('\\') > -1) {
            tokens = tokens.concat(term.split('\\'));
        } else if (term.indexOf('_') > 0) {
            tokens = tokens.concat(term.split('_'));
        }

        // Merge in splitting the string by case and return
        tokens = tokens.concat(term.match(/(([A-Z]?[^A-Z]*)|([a-z]?[^a-z]*))/g).slice(0,-1));

        return tokens;
    };

    root.Sami = {
        /**
         * Cleans the provided term. If no term is provided, then one is
         * grabbed from the query string "search" parameter.
         */
        cleanSearchTerm: function(term) {
            // Grab from the query string
            if (typeof term === 'undefined') {
                var name = 'search';
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
                var results = regex.exec(location.search);
                if (results === null) {
                    return null;
                }
                term = decodeURIComponent(results[1].replace(/\+/g, " "));
            }

            return term.replace(/<(?:.|\n)*?>/gm, '');
        },

        /** Searches through the index for a given term */
        search: function(term) {
            // Create a new search index if needed
            if (!bhIndex) {
                bhIndex = new Bloodhound({
                    limit: 500,
                    local: searchIndex,
                    datumTokenizer: function (d) {
                        return tokenizer(d.name);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                bhIndex.initialize();
            }

            results = [];
            bhIndex.get(term, function(matches) {
                results = matches;
            });

            if (!rootPath) {
                return results;
            }

            // Fix the element links based on the current page depth.
            return $.map(results, function(ele) {
                if (ele.link.indexOf('..') > -1) {
                    return ele;
                }
                ele.link = rootPath + ele.link;
                if (ele.fromLink) {
                    ele.fromLink = rootPath + ele.fromLink;
                }
                return ele;
            });
        },

        /** Get a search class for a specific type */
        getSearchClass: function(type) {
            return searchTypeClasses[type] || searchTypeClasses['_'];
        },

        /** Add the left-nav tree to the site */
        injectApiTree: function(ele) {
            ele.html(treeHtml);
        }
    };

    $(function() {
        // Modify the HTML to work correctly based on the current depth
        rootPath = $('body').attr('data-root-path');
        treeHtml = treeHtml.replace(/href="/g, 'href="' + rootPath);
        Sami.injectApiTree($('#api-tree'));
    });

    return root.Sami;
})(window);

$(function() {

    // Enable the version switcher
    $('#version-switcher').change(function() {
        window.location = $(this).val()
    });

    
        // Toggle left-nav divs on click
        $('#api-tree .hd span').click(function() {
            $(this).parent().parent().toggleClass('opened');
        });

        // Expand the parent namespaces of the current page.
        var expected = $('body').attr('data-name');

        if (expected) {
            // Open the currently selected node and its parents.
            var container = $('#api-tree');
            var node = $('#api-tree li[data-name="' + expected + '"]');
            // Node might not be found when simulating namespaces
            if (node.length > 0) {
                node.addClass('active').addClass('opened');
                node.parents('li').addClass('opened');
                var scrollPos = node.offset().top - container.offset().top + container.scrollTop();
                // Position the item nearer to the top of the screen.
                scrollPos -= 200;
                container.scrollTop(scrollPos);
            }
        }

    
    
        var form = $('#search-form .typeahead');
        form.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'search',
            displayKey: 'name',
            source: function (q, cb) {
                cb(Sami.search(q));
            }
        });

        // The selection is direct-linked when the user selects a suggestion.
        form.on('typeahead:selected', function(e, suggestion) {
            window.location = suggestion.link;
        });

        // The form is submitted when the user hits enter.
        form.keypress(function (e) {
            if (e.which == 13) {
                $('#search-form').submit();
                return true;
            }
        });

    
});


