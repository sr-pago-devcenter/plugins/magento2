<?php

/*
 * Title:       SrPago Payment Gateway
 * Author:      Roberto Ramírez
 * URL:         https://www.srpago.com
 */

namespace Srpago\SrpagoPayments\Block\Info;

use Magento\Payment\Block\Info;

class Custom extends Info
{
    protected $_template = 'Srpago_SrpagoPayments::info/custom.phtml';

    public function getOfflineInfo()
    {
        return $this->getMethod()
            ->getInfoInstance()
            ->getAdditionalInformation('offline_info');
    }

    public function getInstructions()
    {
        return $this->getMethod()->getInstructions();
    }
}
