<?php

/*
 * Title:       SrPago Payment Gateway
 * Author:      Roberto Ramírez
 * URL:         https://www.srpago.com
 */

namespace Srpago\SrpagoPayments\Model\Ui;

use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Escaper;
use Magento\Payment\Helper\Data as PaymentHelper;
use Srpago\SrpagoPayments\Model\Cash;
use Srpago\SrpagoPayments\Model\Source\CashStores;

class CashConfigProvider implements ConfigProviderInterface
{
    /**
     * @var string[]
     */
    protected $methodCode = Cash::CODE;

    /**
     * @var Reference
     */
    protected $method;

    /**
     * @var Escaper
     */
    protected $escaper;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    /**
     * @param PaymentHelper $paymentHelper
     * @param Escaper $escaper
     */
    public function __construct(
        PaymentHelper $paymentHelper,
        Escaper $escaper,
        Cart $cart
    ) {
        $this->escaper = $escaper;
        $this->method = $paymentHelper->getMethodInstance($this->methodCode);
        $this->cart = $cart;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $config = [
            'payment' => [
                'srpago' => [
                    'cash_form' => [
                        'instructions' => $this->getInstructions(),
                        'stores' => CashStores::toArray(),
                    ],
                ],
            ],
        ];

        return $this->method->isAvailable() ? $config : [];
    }

    /**
     * Get instructions from config
     *
     * @return string
     */
    protected function getInstructions()
    {
        return nl2br($this->escaper->escapeHtml($this->method->getInstructions()));
    }
}
