/**
 * Title:       SrPago Payment Gateway
 * Author:      Roberto Ramírez
 * URL:         https://www.srpago.com
 */
/*browser:true*/
/*global define*/
define(
    [
        'Magento_Payment/js/view/payment/cc-form',
        'jquery',
        'Magento_Checkout/js/model/quote',
        'Magento_Customer/js/model/customer',
        'Magento_Payment/js/model/credit-card-validation/validator',
        'SrPago',
        'SrPagoEncryption'
    ],
    function(Component, $, quote, customer, validator) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Srpago_SrpagoPayments/payment/card-form'
            },

            getCode: function() {
                return 'srpagopayments_card';
            },

            isActive: function() {
                return true;
            },

            activeMonthlyInstallments: function() {
                return window.checkoutConfig.payment.srpago.active_monthly_installments;
            },

            getMonthlyInstallments: function() {
                var months = [];
                var i = 0;

                for (i in window.checkoutConfig.payment.srpago.monthly_installments) {
                    months.push(window.checkoutConfig.payment.srpago.monthly_installments[i]);
                }

                return months.sort(function(a, b) { return a - b; });
            },

            getMinimumAmountMonthlyInstallments: function() {
                return window.checkoutConfig.payment.srpago.minimum_amount_monthly_installments;
            },

            getTotal: function() {
                return parseFloat(window.checkoutConfig.payment.total);
            },

            validate: function() {
                var $form = $('#' + this.getCode() + '-form');

                return $form.validation() && $form.validation('isValid');
            },

            preparePayment: function() {
                var self = this;
                var $form = $('#' + this.getCode() + '-form');

                SrPago.setPublishableKey(window.checkoutConfig.payment.srpago.publicKey);
                SrPago.setLiveMode(! window.checkoutConfig.payment.srpago.isSandbox);

                if ($form.validation() && $form.validation('isValid')) {
                    self.messageContainer.clear();

                    if (! this.validateMonthlyInstallments()) {
                        self.messageContainer.addErrorMessage({
                            message: 'El monto requerido para los pagos diferidos no es válido.'
                        });
                    }

                    var onSuccessHandler = function (response) {
                        $form.find('#card_token').val(response.token);

                        self.placeOrder();
                    };

                    var onFailHandler = function (response) {
                        self.messageContainer.addErrorMessage({
                            message: response.message
                        });
                    };

                    var cardData = {
                        number:         this.creditCardNumber(),
                        holder_name:    this.getHolderName(),
                        cvv:            this.creditCardVerificationNumber(),
                        exp_month:      this.creditCardExpMonth(),
                        exp_year:       this.creditCardExpYear().replace(/ /g, ''),
                    };

                    SrPago.token.create(cardData, onSuccessHandler, onFailHandler);
                } else {
                    return $form.validation() && $form.validation('isValid');
                }
            },


            getHolderName: function() {
                return $("#card_holder_name").val();
            },

            validateMonthlyInstallments: function() {
                if (this.activeMonthlyInstallments() && isNaN(installments) == false) {
                    var totalOrder = this.getTotal();
                    if (totalOrder >= this.getMinimumAmountMonthlyInstallments()) {
                        var installments = parseInt($('#srpago_monthly_installments').val());
                        if (installments == 1) {
                            return true;
                        } else {
                            return (installments * 100 < totalOrder);
                        }
                    } else {
                        return false;
                    }
                }

                return true;
            },

            initObservable: function() {
                console.log('__initObservable__');
                this._super()
                    .observe([
                        'transactionResult'
                    ]);
                return this;
            },

            getData: function() {
                var number = this.creditCardNumber().replace(/\D/g,'');
                var data = {
                    'method': this.getCode(),
                    'additional_data': {
                        'cc_type': this.creditCardType(),
                        'cc_exp_year': this.creditCardExpYear(),
                        'cc_exp_month': this.creditCardExpMonth(),
                        'cc_bin': number.substring(0, 6),
                        'cc_last_4': number.substring(number.length-4, number.length),
                        'card_token': $("#card_token").val()
                    }
                };

                if (this.activeMonthlyInstallments()) {
                    data['additional_data']['monthly_installments'] = $('#srpago_monthly_installments').val();
                }

                return data;
            },

            getTransactionResults: function() {
                console.log('__getTransactionResults__');
                return _.map(window.checkoutConfig.payment.srpago_gateway.transactionResults, function(value, key) {
                    return {
                        'value': key,
                        'transaction_result': value
                    }
                });
            }
        });
    }
);
