<?php

/*
 * Title:       SrPago Payment Gateway
 * Author:      Roberto Ramírez
 * URL:         https://www.srpago.com
 */

namespace Srpago\SrpagoPayments\Controller\Webhook;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment\Transaction;

class Listener extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    protected $request;

    protected $logger;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        Http $request,
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);

        $this->request = $request;
        $this->resultPageFactory = $resultPageFactory;

        $this->logger = ObjectManager::getInstance()
            ->get(\Psr\Log\LoggerInterface::class);

        // CsrfAwareAction Magento2.3 compatibility
        if (interface_exists('\Magento\Framework\App\CsrfAwareActionInterface')) {
            if (
                $this->request instanceof Http &&
                $this->request->isPost() &&
                empty($this->request->getParam('form_key'))
            ) {
                $formKey = $this->_objectManager->get(\Magento\Framework\Data\Form\FormKey::class);
                $request->setParam('form_key', $formKey->getFormKey());
            }
        }
    }

    public function execute()
    {
        $body = $this->request->getContent();
        $data = json_decode($body, true);

        if (isset($data['operation']) && 'POS' != $data['operation']['payment_method']) {
            try {
                $data = $data['operation'];

                $this->logger->debug('srpago.webhook', $data);

                $order_id = explode(',', $data['reference']['description']);
                $order_id = (int) substr(trim($order_id[0]), 1);

                $order = $this->_objectManager->create(\Magento\Sales\Model\Order::class);
                $order->loadByIncrementId($order_id);

                if (! empty($order->getIncrementId())) {
                    $order->setState(Order::STATE_PROCESSING);
                    $order->setStatus(Order::STATE_PROCESSING);

                    $order->setExtOrderId($data['transaction']);
                    $order->setTotalPaid($data['total']['amount']);

                    $order->addStatusHistoryComment('Pago recibido exitosamente. ID: ' . $data['transaction']);
                    $order->setIsCustomerNotified(true);

                    $order->save();

                    $payment = $order->getPayment();
                    $payment
                        ->setAdditionalInformation('webhook', $data)
                        ->setTransactionId($data['transaction'])
                        ->setIsTransactionClosed(true)
                        ->save()
                    ;

                    $transaction = $payment->addTransaction(Transaction::TYPE_PAYMENT);
                    $transaction->save();
                }
            } catch (\Exception $e) {
                $this->logger->critical(
                    'srpago.CRITICAL',
                    [
                        'message' => 'Error processing webhook notification',
                        'exception' => $e,
                    ]
                );
            }
        }

        header('HTTP/1.1 200 OK');
        exit;
    }
}
