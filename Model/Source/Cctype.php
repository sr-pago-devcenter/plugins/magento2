<?php

/*
 * Title:       SrPago Payment Gateway
 * Author:      Roberto Ramírez
 * URL:         https://www.srpago.com
 */

namespace Srpago\SrpagoPayments\Model\Source;

class Cctype extends \Magento\Payment\Model\Source\Cctype
{
    /**
     * Tipos de tarjetas que son soportadas.
     *
     *  @return array
     */
    public function getAllowedTypes()
    {
        return [
        	'VI', 'MC', 'AE'
        ];
    }
}
