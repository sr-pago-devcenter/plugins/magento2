<?php

/*
 * Title:       SrPago Payment Gateway
 * Author:      Roberto Ramírez
 * URL:         https://www.srpago.com
 */

namespace Srpago\SrpagoPayments\Model\Ui;

use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Payment\Helper\Data as PaymentHelper;
use Srpago\SrpagoPayments\Model\Card as SrpagoCardPayment;

class ConfigProvider implements ConfigProviderInterface
{
    /**
     * Código del metodo de pago.
     *
     * @var array
     */
    protected $methodCodes = [
        'srpagopayments_card',
    ];

    /**
     * @var \Magento\Payment\Model\Method\AbstractMethod[]
     */
    protected $methods = [];

    /**
     * @var \Srpago\SrpagoPayments\Model\Card
     */
    protected $paymentCard;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;


    /**
     * @param PaymentHelper $paymentHelper
     * @param \Srpago\SrpagoPayments\Model\Card $paymentCard
     */
    public function __construct(PaymentHelper $paymentHelper, SrpagoCardPayment $paymentCard, Cart $cart)
    {
        foreach ($this->methodCodes as $code) {
            $this->methods[$code] = $paymentHelper->getMethodInstance($code);
        }

        $this->cart = $cart;
        $this->paymentCard = $paymentCard;
    }

    /**
     * Regresa la configuración necesaria para la vista.
     *
     * @return object \SrPago\Model\ConfigProvider
     */
    public function getConfig()
    {
        $config = [];

        foreach ($this->methodCodes as $code) {
            if ($this->methods[$code]->isAvailable()) {
                $config['payment']['srpago']['publicKey'] = $this->paymentCard->getPublicKey();
                $config['payment']['srpago']['isSandbox'] = $this->paymentCard->getIsSandbox();

                $config['payment']['ccform']["availableTypes"][$code] = $this->paymentCard->getActiveTypeCards();

                $config['payment']['srpago']['active_monthly_installments'] = $this->paymentCard->isActiveMonthlyInstallments();
                if ($config['payment']['srpago']['active_monthly_installments']) {
                    $config['payment']['srpago']['monthly_installments'] = $this->paymentCard->getMonthlyInstallments();
                    $config['payment']['srpago']['minimum_amount_monthly_installments'] = $this->paymentCard->getMinimumAmountMonthlyInstallments();
                }

                $config['payment']['total'] = $this->cart->getQuote()->getGrandTotal();

                $config['payment']['ccform']["hasVerification"][$code] = true;
                $config['payment']['ccform']["hasSsCardType"][$code] = false;
                $config['payment']['ccform']["months"][$code] = $this->getMonths();
                $config['payment']['ccform']["years"][$code] = $this->getYears();
                $config['payment']['ccform']["cvvImageUrl"][$code] = "https://www.ekwb.com/shop/skin/frontend/base/default/images/cvv.gif";
                $config['payment']['ccform']["ssStartYears"][$code] = $this->_getStartYears();
            }
        }

        return $config;
    }

    /**
     * Regresa la lista de meses.
     *
     * @return array
     */
    private function getMonths()
    {
        return [
            '1'     => '01 - Enero',
            '2'     => '02 - Febrero',
            '3'     => '03 - Marzo',
            '4'     => '04 - Abril',
            '5'     => '05 - Mayo',
            '6'     => '06 - Junio',
            '7'     => '07 - Julio',
            '8'     => '08 - Augosto',
            '9'     => '09 - Septiembre',
            '10'    => '10 - Octubre',
            '11'    => '11 - Noviembre',
            '12'    => '12 - Diciembre',
        ];
    }

    /**
     * Regresa una lista con los años para el formulario.
     *
     * @return array
     */
    private function getYears($start_year = null, $end_year = 10)
    {
        $start_year = $start_year ?: date('Y');
        $end_year = $start_year + ($end_year ?: 10);

        $years = range($start_year, $end_year);
        $years = array_combine($years, $years);

        return $years;
    }

    /**
    * @return array
    */
    private function _getStartYears()
    {
        $years = [];
        $cYear = (integer) date("Y");

        for($i=5; $i>=0; $i--) {
            $year = (string)($cYear - $i);
            $years[$year] = $year;
        }

        return $years;
    }
}
