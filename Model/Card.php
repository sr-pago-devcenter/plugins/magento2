<?php

/*
 * Title:       SrPago Payment Gateway
 * Author:      Roberto Ramírez
 * URL:         https://www.srpago.com
 */

namespace Srpago\SrpagoPayments\Model;

use Magento\Directory\Model\CountryFactory;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Framework\Registry;
use Magento\Framework\Phrase;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Payment\Helper\Data;
use Magento\Payment\Model\Method\Cc;
use Magento\Payment\Model\Method\Logger;
use Magento\Store\Api\Data\StoreInterface;
use SrPago\Error\SrPagoError;
use SrPago\Http\HttpClient;
use SrPago\SrPago;
use SrPago\SrpagoPayments\Model\Config;
use Srpago\SrpagoPayments\Util\SrPagoMisc;

class Card extends Cc
{
    /**
     * Código del metodo de pago.
     *
     * @var string
     */
    const CODE = 'srpagopayments_card';

    /**
     * Monto minimo, por default, para cargos diferidos.
     *
     * @var float
     */
    const MINAMOUNT = 300.00;

    protected $_code = self::CODE;

    protected $_isGateway = true;

    /**
     * Esta habilitada la captura de los cargos.
     *
     * @var bool
     */
    protected $_canCapture = true;

    protected $countryFactory;

    /**
     * Monto minimo para los cargos.
     *
     * @var float
     */
    protected $minimumAmount = null;

    /**
     * Monedas soportadas.
     *
     * @var string
     */
    protected $supportedCurrencyCodes = [ 'MXN', ];

    /**
     * Esta habilitado el modo sandbox
     *
     * @var bool
     */
    protected $isSandbox = true;

    /**
     * SrPago - App Key
     *
     * @var string
     */
    protected $appKey = null;

    /**
     * SrPago - App Secret
     *
     * @var string
     */
    protected $appSecret = null;

    /**
     * SrPago - Public Key
     *
     * @var string
     */
    protected $publicKey = null;

    /**
     * Meses en los que se puede diferir un cargo.
     *
     * @var string
     */
    protected $monthlyInstallments;

    /**
     * Indica si los cargos diferidos estan activos.
     *
     * @var string
     */
    protected $activeMonthlyInstallments;

    /**
     * Monto minimo para cargos diferidos.
     *
     * @var float
     */
    protected $minAmountMonthInstallments;

    /**
     * Catálogo de los tipos de tarjetas.
     *
     * @var float
     */
    protected $typesCards;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger $logger
     * @param \Magento\Framework\Module\ModuleListInterface $moduleList
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        Data $paymentData,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        ModuleListInterface $moduleList,
        TimezoneInterface $localeDate,
        CountryFactory $countryFactory,
        array $data = array()
    )
    {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $moduleList,
            $localeDate,
            null,
            null,
            $data
        );

        if (! class_exists('\\Srpago\\SrpagoPayments\\Model\\Config')) {
            throw new \Magento\Framework\Validator\Exception(
                __("Class Srpago\\SrpagoPayments\\Model\\Config not found.")
            );
        }

        $this->isSandbox = (boolean) $this->getSrpagoConfig('sandbox_mode');

        $prefix = $this->isSandbox ? 'sandbox' : 'live';
        $appKey = (string) $this->getSrpagoConfig($prefix . '_app_key');
        $appSecret = (string) $this->getSrpagoConfig($prefix . '_app_secret');
        $publicKey = (string) $this->getSrpagoConfig($prefix . '_public_key');

        if (! empty($appKey)) {
            $this->appKey = $appKey;
            unset($appKey);
        } else {
            $this->_logger->error(__('Please set SrPago App Key in your admin.'));
        }

        if (! empty($appSecret)) {
            $this->appSecret = $appSecret;
            unset($appSecret);
        } else {
            $this->_logger->error(
                __('Please set SrPago App Secret in your admin.')
            );
        }

        if (! empty($publicKey)) {
            $this->publicKey = $publicKey;
            unset($publicKey);
        } else {
            $this->_logger->error(
                __('Please set SrPago Public Key in your admin.')
            );
        }

        $this->activeMonthlyInstallments =
            ((integer) $this->getConfigData(
                'active_monthly_installments'
            ));

        if ($this->activeMonthlyInstallments) {
            $this->monthlyInstallments = $this->getConfigData(
                'monthly_installments'
            );

            $this->minAmountMonthInstallments =
                (float) $this->getConfigData(
                    'minimum_amount_monthly_installments'
                );
            if (empty($this->minAmountMonthInstallments)
                || $this->minAmountMonthInstallments <= 0) {
                $this->minAmountMonthInstallments = self::MINAMOUNT;
            }
        }

        $this->minimumAmount = $this->getConfigData('minimum_amount');
        $this->countryFactory = $countryFactory;
        $this->typesCards = $this->getConfigData('cctypes');
    }

    /**
     * Regresa si el modo sandbox esta activo o no.
     *
     * @return boolean
     */
    public function getIsSandbox()
    {
        return $this->isSandbox;
    }

    /**
     * Regresa la public_key
     *
     * @return string
     */
    public function getPublicKey()
    {
        return $this->publicKey;
    }

    /**
     * Regresa los tipos de tarjetas soportados.
     *
     * @return array
     */
    public function getActiveTypeCards()
    {
        $activeTypes = explode(',', $this->typesCards);
        $supportType = [
            'AE' => 'American Express',
            'VI' => 'Visa',
            'MC' => 'MasterCard',
        ];

        $return = [];
        foreach ($activeTypes AS $value) {
            $return[$value] = $supportType[$value];
        }

        return $return;
    }

    /**
     * Regresa si los pagos a meses sin intereses estan activos.
     *
     * @return boolean
     */
    public function isActiveMonthlyInstallments()
    {
        return $this->activeMonthlyInstallments;
    }

    /**
     * Regresa los meses soportados para pagos diferidos.
     *
     * @return array
     */
    public function getMonthlyInstallments()
    {
        $months = explode(',', $this->monthlyInstallments);

        if (! in_array('1', $months)) {
            array_push($months, '1');
        }
        asort($months);

        return $months;
    }

    /**
     * Regresa el monto minimo que debe alcanzar una orden para pagos diferidos.
     *
     * @return float
     */
    public function getMinimumAmountMonthlyInstallments()
    {
        return $this->minAmountMonthInstallments;
    }

    /**
     * Determina la disponibilidad del método en función del monto de la orden
     * y los datos de configuración
     *
     * @param \Magento\Quote\Api\Data\CartInterface|null $quote
     * @return bool
     */
    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        if ($quote) {
            if ($quote->getBaseGrandTotal() < $this->minimumAmount) {
                return false;
            }
        }

        if (empty($this->appKey) || empty($this->appSecret) || empty($this->publicKey)) {
            return false;
        }

        return parent::isAvailable($quote);
    }

    /**
     * Determina la disponibilidad en base al tipo de moneda.
     *
     * @param string $currencyCode
     * @return bool
     */
    public function canUseForCurrency($currencyCode)
    {
        return in_array($currencyCode, $this->supportedCurrencyCodes);
    }


    /**
     * Asignar información del pago a la orden.
     *
     * @param \Magento\Framework\DataObject|mixed $data
     * @return $this
     * @throws \Exception
     */
    public function assignData(\Magento\Framework\DataObject $data)
    {
        parent::assignData($data);

        $content = (array) $data->getData();

        $info = $this->getInfoInstance();

        if (key_exists('additional_data', $content)) {
            if (key_exists('card_token',$content['additional_data'])) {
                $additionalData = $content['additional_data'];

                $info->setAdditionalInformation('card_token', $additionalData['card_token']);
                $info
                    ->setCcType($additionalData['cc_type'])
                    ->setCcExpYear($additionalData['cc_exp_year'])
                    ->setCcExpMonth($additionalData['cc_exp_month'])
                ;

                if (key_exists('monthly_installments', $additionalData)) {
                    $info->setAdditionalInformation(
                        'monthly_installments',
                        $additionalData['monthly_installments']
                    );
                }

                $info->setAdditionalInformation(
                    'cc_bin',
                    $additionalData['cc_bin']
                );
                $info->setAdditionalInformation(
                    'cc_last_4',
                    $additionalData['cc_last_4']
                );
            } else {
                $this->_logger->error(__('[SrPago]: Card token not found.'));
                throw new \Magento\Framework\Validator\Exception(
                    __('Error al capturar el pago')
                );
            }

            if ($this->isActiveMonthlyInstallments()) {
                if (key_exists(
                    'monthly_installments',
                    $content['additional_data'])){
                    $info->setAdditionalInformation(
                        'monthly_installments',
                        $content['additional_data']['monthly_installments']
                    );
                }
            }

            return $this;
        }

        throw new \Magento\Framework\Validator\Exception(
            __('Error al capturar el pago')
        );
    }

    /**
     * Payment capturing
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws \Magento\Framework\Validator\Exception
     */
    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        Config::initializeSrPagoLibrary($this->isSandbox, $this->appKey, $this->appSecret);

        $info = $this->getInfoInstance();
        $order = $payment->getOrder();
        $monthlyInstallments = $info->getAdditionalInformation(
            'monthly_installments'
        );

        $chargeRequest['amount'] = $amount;
        $chargeRequest['description'] = sprintf('#%s, %s', $order->getIncrementId(), $order->getCustomerEmail());
        $chargeRequest['reference'] = $order->getIncrementId();
        $chargeRequest['ip'] = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\HTTP\PhpEnvironment\RemoteAddress')
            ->getRemoteAddress();

        $chargeRequest['metadata']['billing'] = Config::getBillingInfo($order);
        // $chargeRequest['metadata']['coupons'] = Config::getDiscountInfo($order);
        $chargeRequest['metadata']['member'] = Config::getCustomerInfo($order);
        $chargeRequest['metadata']['shipping'] = Config::getShippingInfo($order);
        $chargeRequest['metadata']['items'] = Config::getOrderItemsInfo($order);

        if ($this->isActiveMonthlyInstallments() && 1 < intval($monthlyInstallments)) {
            if ($amount >= $this->getMinimumAmountMonthlyInstallment()) {
                $chargeRequest['months'] = $monthlyInstallments;

                $order->addStatusHistoryComment(
                    sprintf('La compra ha sido diferida a %s meses.', $chargeRequest['months'])
                );
                $order->save();
            } else {
                $this->_logger->error(
                    __(sprintf('[SrPago]: Plazos: %s Monto: %s', $monthlyInstallments, $amount))
                );

                throw new \Magento\Framework\Validator\Exception(
                    __('La orden no alcanza el monto mínimo para los pagos diferidos.')
                );
            }
        }

        $chargeRequest['source'] = $this->getInfoInstance()->getAdditionalInformation('card_token');

        try {
            $SrPagoCharge = SrPago::Charges();
            $chargeResponse = $SrPagoCharge->create($chargeRequest);

            $chargeResponse = SrPagoMisc::parseResponse($chargeResponse, HttpClient::$last);

            $payment
                ->setTransactionId($chargeResponse['recipe']['transaction'])
                ->setIsTransactionClosed(true)
            ;
        } catch(SrPagoError $e) {
            $error = $e->getError();

            $this->_logger->error('Error al procesar la transacción.', $error);

            switch ($error['code']) {
                case 'InvalidParamException':
                case 'InvalidEncryptionException':
                case 'PaymentFilterException':
                case 'SwitchException':
                case 'InternalErrorException':
                default:
                    $error = new Phrase('Ha ocurrido un error al procesar la transacción, favor de volverlo a intentar mas tarde.');
                break;
                case 'SuspectedFraudException':
                case 'InvalidTransactionException':
                case 'PaymentException':
                    $error = new Phrase('La transacción ha sido denegada por el banco emisor.');
                break;
            }

            throw new \Magento\Framework\Validator\Exception($error);
        } catch(\Exception $e) {
            $this->_logger->error(
                __('[SrPago]: Error al intentar capturar la orden. ' . $e->getMessage())
            );

            throw new \Magento\Framework\Validator\Exception(__(
                    $e->getMessage()
                )
            );
        }

        return $this;
    }

    /**
     * Recupera información de la configuración general.
     *
     * @return mixed
     */
    private function getSrpagoConfig($field)
    {
        $path = 'payment/' . \Srpago\SrpagoPayments\Model\Config::CODE . '/' . $field;

        return $this
            ->_scopeConfig
            ->getValue(
                $path,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                parent::getStore()
            )
        ;
    }

    /**
     * Validacion de los datos para el pago.
     */
    public function validate()
    {
        $info = $this->getInfoInstance();
        $errorMsg = false;
        $availableTypes = explode(',', $this->getConfigData('cctypes'));

        // PCI assurance
        $binNumber = $info->getAdditionalInformation('cc_bin');
        $last4 =  $info->getAdditionalInformation('cc_last_4');
        $ccNumber = $binNumber . $last4;

        // remove credit card number delimiters such as "-" and space
        $ccNumber = preg_replace('/[\-\s]+/', '', $ccNumber);
        $info->setCcNumber($ccNumber . "******" . $last4);

        $ccType = '';

        if (in_array($info->getCcType(), $availableTypes)) {
            if ($this->validateCcNumOther($binNumber)) {
                $ccTypeRegExpList = [
                    'VI' => '/^4[0-9]{12}([0-9]{3})?$/',
                    'MC' => '/^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$/',
                    'AE' => '/^3[47][0-9]{13}$/',
                ];

                $ccNumAndTypeMatches = isset(
                    $ccTypeRegExpList[$info->getCcType()]
                ) && preg_match(
                    $ccTypeRegExpList[$info->getCcType()],
                    $ccNumber
                ) || !isset(
                    $ccTypeRegExpList[$info->getCcType()]
                );

                $ccType = $ccNumAndTypeMatches ? $info->getCcType() : 'OT';
            } else {
                $errorMsg = __('Custom Invalid Credit Card Number');
            }
        } else {
            $errorMsg = __(
                'Custom This credit card type is not allowed for this payment method.'
            );
        }

        if ($ccType != 'SS' && !$this->_validateExpDate($info->getCcExpYear(), $info->getCcExpMonth())) {
            $errorMsg = __('Please enter a valid credit card expiration date.');
        }

        if ($errorMsg) {
            throw new \Magento\Framework\Exception\LocalizedException($errorMsg);
        }

        return $this;
    }
}
