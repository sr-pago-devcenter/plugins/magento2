<?php

/**
 * Title:       SrPago Payment Gateway
 * Author:      Roberto Ramírez
 * URL:         https://www.srpago.com
 */

namespace Srpago\SrpagoPayments\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;
use Srpago\SrpagoPayments\Model\Cash;

class BeforeOrderPaymentCashSaveObserver implements ObserverInterface
{
    private $logger;

    private $inputParamsResolver;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Webapi\Controller\Rest\InputParamsResolver $inputParamsResolver)
    {
        $this->logger = $logger;
        $this->inputParamsResolver = $inputParamsResolver;
    }

    /**
     * Sets current instructions for bank transfer account
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $payment = $observer->getEvent()->getPayment();
        $order = $payment->getOrder();
        $payment_method = $payment->getMethodInstance();

        if (
            $payment->getMethod() == Cash::CODE &&
            Order::STATE_NEW == $order->getState() &&
            $payment_method->getDefaultStatus() == $order->getStatus() &&
            null == $order->getExtOrderId()
        ) {
            $inputParams = $this->inputParamsResolver->resolve();
            foreach ($inputParams as $inputParam) {
                if ($inputParam instanceof \Magento\Quote\Model\Quote\Payment) {
                    $paymentData = $inputParam->getData('additional_data');

                    $response = $payment_method
                        ->createPaymentOrder($payment, $paymentData['store']);

                    $this->logger->debug('srpago.charge_response', $response);

                    $payment->setAdditionalInformation('charge_response', $response);
                }
            }
        }
    }
}
