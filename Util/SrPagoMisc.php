<?php

/**
 * Title:       SrPago Payment Gateway
 * Author:      Roberto Ramírez
 * URL:         https://www.srpago.com
 */

namespace Srpago\SrpagoPayments\Util;

use SrPago\Error\SrPagoError;
use SrPago\Http\HttpClient;

class SrPagoMisc
{
    public static function parseResponse($response, $last_request)
    {
        $httpCode = (
            isset($last_request['info']) &&
            isset($last_request['info']['http_code'])
        ) ?
            $last_request['info']['http_code'] :
            0;

        if (
            is_array($response) &&
            isset($response['success']) &&
            true == $response['success']
        ) {
            return isset($response['result']) ?
                $response['result'] :
                (
                    isset($response['connection']) ?
                        $response['connection'] :
                        null
                );
        }

        if (! isset($response['error'])) {
            $response['error'] = array(
                'code' => 'CommunicationException',
                'message' => 'Hubo un problema al establecer la conexión con Sr. Pago'
            );
        }

        $error = new SrPagoError($response['error']['code'], $httpCode);
        $error->setError($response['error']);

        throw $error;
    }
}
