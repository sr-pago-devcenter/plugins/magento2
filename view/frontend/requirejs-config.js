var config = {
    paths: {
        'SrPago': 'Srpago_SrpagoPayments/js/srpago.min',
        'SrPagoEncryption': 'Srpago_SrpagoPayments/js/srpago.encryption.min',
        'JSEncrypt': 'Srpago_SrpagoPayments/js/jsencrypt',
        'aesjs': 'Srpago_SrpagoPayments/js/aesjs'
    },
    shim: {
        'jquery' : {
            exports : '$'
        },
        'SrPago': {
            'deps': [ 'jquery' ]
        },
        'SrPagoEncryption': {
            'deps': [ 'SrPago', 'JSEncrypt', 'aesjs' ]
        }
    }
};
