<?php

/*
 * Title:       SrPago Payment Gateway
 * Author:      Roberto Ramírez
 * URL:         https://www.srpago.com
 */

namespace Srpago\SrpagoPayments\Model;

use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Sales\Model\Order;
use SrPago\SrPago;

define('SRPAGO_RSA_PUBLIC_KEY', '-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAv0utLFjwHQk+1aLjxl9t
Ojvt/qFD1HfMFzjYa4d3iFKrQtvxaWM/B/6ltPn6+Pez+dOd59zFmzNHg33h8S0p
aZ6wmNv3mwp4hCJttGzFvl2hhw8Z+OU9KwGSXgQ+5FNyRyDLp0qt75ayvV0vV8oX
0Pgubd/NTHzRKk0ubXO8WVWkNhMdsv0HGrhIMDXAWLAQBzDewmICVH9MIJzjoZym
R7AuNpefD4hoVK8cBMjZ0xRKSPyd3zI6uJyERcR3+N9nxvg4guShP27cnD9qpLt4
L6YtU0BU+husFXoHL6Y2CsxyzxT9mtorAGe5oRiTC7Z/S9u7pxGN4iozgmAei0MZ
VbKows/qa9/q0PPzbF/PHSZKou1DJvsJ2PKY3ZPYAT7/u4x8NRiJ/6cssuzsIPUd
Q9HBzA1ZBMHkpOmkipu1G7ks/GwTfQJkHPW5xHu1EOYvgv/PHr3BJnCMNYKFvf5c
4Qd0COnnU3jDel1OKl7lUzr+ioqUedX393D/fszdK4hjvtUjo6ThTRNm3y4avY/r
m+oLu8sZWpyBm4PfN2xGOnFco9SiyCT03XOEuOXokid6BDMi0aue9LKJaQR+KGVc
/H2p2d2Yu4GdgXS1vq1syaf7V0QPOmamTOyJRZ45UoLfBRB8nYBGDo0mPR7GIon6
M8SmGGsTo3V0L+Ni9bNJHa8CAwEAAQ==
-----END PUBLIC KEY-----');

class Config extends AbstractMethod
{
    const CODE = 'srpagopayments_config';

    protected $_code = self::CODE;

    /**
     * @throws \Magento\Framework\Validator\Exception
     */
    public static function initializeSrPagoLibrary($isSandbox, $appKey, $appSecret)
    {
        try {
            if (empty($appKey) || empty($appSecret)) {
                throw new \Magento\Framework\Validator\Exception(
                    __('Favor de revisar la configuración del método de pago SrPago')
                );
            }

            SrPago::setLiveMode(! $isSandbox);
            SrPago::setApiKey($appKey);
            SrPago::setApiSecret($appSecret);
        }catch(\Exception $e){
            throw new \Magento\Framework\Validator\Exception(
                __($e->getMessage())
            );
        }
    }

    /**
     * Regresa la información de facturación de la orden.
     *
     * @param Magento\Sales\Model\Order $order
     * @return array
     */
    public static function getBillingInfo(Order $order)
    {
        $billing = $order->getBillingAddress()->getData();

        $billingInfo = [
            'billingEmailAddress'   => $order->getCustomerEmail(),
            'billingFirstName-D'    => $billing['firstname'],
            'billingLastName-D'     => $billing['lastname'],
            // 'billingAddress-D'      => $this->order->get_billing_address_1(),
            // 'billingAddress2-D'     => $this->order->get_billing_address_2(),
            // 'billingCity-D'         => $this->order->get_billing_city(),
            // 'billingState-D'        => $this->order->get_billing_state(),
            // 'billingPostalCode-D'   => $this->order->get_billing_postcode(),
            // 'billingCountry-D'      => $this->order->get_billing_country(),
            'billingPhoneNumber-D'  => $billing['telephone'],
        ];

        return $billingInfo;
    }

    /**
     * Regresa la información de los descuentos de la orden.
     *
     * @param Magento\Sales\Model\Order $order
     * @return array
     */
    public static function getDiscountInfo(Order $order)
    {
        $discounts = [];

        foreach ($order->getAllItems() as $item) {
            if (0 < floatval($item->getDiscountAmount())) {
                $description = $order->getDiscountDescription();

                if (empty($description)) {
                    $description = "discount_code";
                }

                $discounts['coupon'][] = [
                    'couponCode'    => $description,
                    'couponAmount'  => abs(intval(strval($order->getDiscountAmount()) * 100)),
                ];
            }
        }

        return $discounts;
    }

    /**
     * Regresa la información del cliente.
     *
     * @param Magento\Sales\Model\Order $order
     * @return array
     */
    public static function getCustomerInfo(Order $order)
    {
        $billing = $order->getBillingAddress();

        $customerInfo = [
            // 'memberId'              => $this->order->get_user_id(),
            // 'membershipDate'        => $user->user_registered,
            'memberFullName'        => sprintf('%s %s',
                $billing->getFirstname(),
                $billing->getLastname()
            ),
            'memberEmailAddress'    => $order->getCustomerEmail(),
            // 'membershipStatus'      => ! $user->user_status,
        ];

        return $customerInfo;
    }

    /**
     * Regresa la información de envio de la orden.
     *
     * @param Magento\Sales\Model\Order $order
     * @return array
     */
    public static function getShippingInfo(Order $order)
    {
        $shippingAddress = $order->getShippingAddress();

        $shippingInfo = [];
        if ($shippingAddress) {
            $billing = $order->getBillingAddress()->getData();
            $shippingData = $shippingAddress->getData();

            $shippingTax = $order->getShippingTaxAmount();
            $shippingCost = $order->getShippingAmount() + $shippingTax;

            $shippingInfo = [
                'shippingCharges'       => (float) $shippingCost,
                'shippingFirstName'     => $shippingData['firstname'],
                'shippingLastName'      => $shippingData['lastname'],
                'shippingAddress'       => $shippingData['street'],
                // 'shippingAddress2'      => esc_html($this->order->get_shipping_address_2()),
                'shippingCity'          => $shippingData['city'],
                'shippingState'         => $shippingData['region'],
                'shippingPostalCode'    => $shippingData['postcode'],
                'shippingCountry'       => $shippingData['country_id'],
                'shippingMethod'        => $order->getShippingMethod(),
            ];
        }

        return $shippingInfo;
    }

    /**
     * Regresa los productos de la orden.
     *
     * @param Magento\Sales\Model\Order $order
     * @return array
     */
    public static function getOrderItemsInfo(Order $order)
    {
        $orderItemsInfo = [];

        $items = $order->getAllVisibleItems();
        foreach ($items as $itemId => $item) {
            if ($item->getProductType() == 'simple' && $item->getPrice() <= 0) {
                break;
            }

            $orderItemsInfo['item'][] = [
                'itemNumber'        => $item->getSku(),
                'itemDescription'   => $item->getName(),
                'itemPrice'         => floatval($item->getPrice()),
                'itemQuantity'          => intval($item->getQtyOrdered()),
            ];
        }

        return $orderItemsInfo;
    }
}
