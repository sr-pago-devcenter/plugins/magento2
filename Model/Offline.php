<?php

/*
 * Title:       SrPago Payment Gateway
 * Author:      Roberto Ramírez
 * URL:         https://www.srpago.com
 */

namespace Srpago\SrpagoPayments\Model;

use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Payment\Helper\Data;
use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Payment\Model\Method\Logger;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Store\Model\ScopeInterface;

class Offline extends AbstractMethod
{
    /**
     * Esta habilitado el modo sandbox
     *
     * @var bool
     */
    protected $isSandbox = true;

    /**
     * SrPago - App Key
     *
     * @var string
     */
    protected $appKey = null;

    /**
     * SrPago - App Secret
     *
     * @var string
     */
    protected $appSecret = null;

    /**
     * SrPago - Public Key
     *
     * @var string
     */
    protected $publicKey = null;

    /**
     * Monto minimo para los cargos.
     *
     * @var float
     */
    protected $minimumAmount = null;

    /**
     * Monedas soportadas.
     *
     * @var string
     */
    protected $supportedCurrencyCodes = [ 'MXN', ];

    protected $_infoBlockType = \Srpago\SrpagoPayments\Block\Info\Custom::class;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;

    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        Data $paymentData,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = array())
    {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data);

        $this->_scopeConfig = $scopeConfig;

        if (! class_exists('\\Srpago\\SrpagoPayments\\Model\\Config')) {
            throw new \Magento\Framework\Validator\Exception(
                __("Class Srpago\\SrpagoPayments\\Model\\Config not found.")
            );
        }

        $this->isSandbox = (boolean) $this->getSrpagoConfig('sandbox_mode');

        $prefix = $this->isSandbox ? 'sandbox' : 'live';
        $appKey = (string) $this->getSrpagoConfig($prefix . '_app_key');
        $appSecret = (string) $this->getSrpagoConfig($prefix . '_app_secret');
        $publicKey = (string) $this->getSrpagoConfig($prefix . '_public_key');

        if (! empty($appKey)) {
            $this->appKey = $appKey;
            unset($appKey);
        } else {
            $this->_logger->error(__('Please set SrPago App Key in your admin.'));
        }

        if (! empty($appSecret)) {
            $this->appSecret = $appSecret;
            unset($appSecret);
        } else {
            $this->_logger->error(
                __('Please set SrPago App Secret in your admin.')
            );
        }

        if (! empty($publicKey)) {
            $this->publicKey = $publicKey;
            unset($publicKey);
        } else {
            $this->_logger->error(
                __('Please set SrPago Public Key in your admin.')
            );
        }

        $this->minimumAmount = $this->getConfigData('minimum_amount');
    }

    /**
     * Determina la disponibilidad del método en función del monto de la orden
     * y los datos de configuración
     *
     * @param \Magento\Quote\Api\Data\CartInterface|null $quote
     * @return bool
     */
    public function isAvailable(CartInterface $quote = null)
    {
        if ($quote && $quote->getBaseGrandTotal() < $this->minimumAmount) {
            return false;
        }

        if (empty($this->appKey) || empty($this->appSecret) || empty($this->publicKey)) {
            return false;
        }

        return parent::isAvailable($quote);
    }

    /**
     * Regresa si el modo sandbox esta activo o no.
     *
     * @return boolean
     */
    public function getIsSandbox()
    {
        return $this->isSandbox;
    }

    /**
     * Regresa la public_key
     *
     * @return string
     */
    public function getPublicKey()
    {
        return $this->publicKey;
    }

    /**
     * Determina la disponibilidad en base al tipo de moneda.
     *
     * @param string $currencyCode
     * @return bool
     */
    public function canUseForCurrency($currencyCode)
    {
        return in_array($currencyCode, $this->supportedCurrencyCodes);
    }

    public function getInstructions()
    {
        return (string) $this->getConfigData('instructions');
    }

    /**
     * Recupera información de la configuración general.
     *
     * @return mixed
     */
    private function getSrpagoConfig($field)
    {
        $path = 'payment/' . \Srpago\SrpagoPayments\Model\Config::CODE . '/' . $field;

        return $this
            ->_scopeConfig
            ->getValue(
                $path,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                parent::getStore()
            )
        ;
    }
}
