define([
    'uiComponent',
    'Magento_Checkout/js/model/payment/renderer-list'
],
function (Component, rendererList) {
    'use strict';

    rendererList.push(
        {
            type: 'srpagopayments_card',
            component: 'Srpago_SrpagoPayments/js/view/payment/method-renderer/srpagopayments-card'
        },
        {
            type: 'srpagopayments_cash',
            component: 'Srpago_SrpagoPayments/js/view/payment/method-renderer/srpagopayments-cash'
        }
    );

    /** Add view logic here if needed */
    return Component.extend({});
});
