# Magento 2 - SrPago Payment Gateway


## VM

* Importar a VirtualBox el archivo .ova
* Iniciar la maquina virtual
* Agregar al archivo hosts la siguiente linea `{ip_vm} magento.srpago.lh` reemplazando {ip_vm} por la ip de la maquina virtual.

#### Accesos

| Usuario | Contraseña | Origen |
| --- | --- | --- |
| srpago | srpago | sistema |
| root | root | sistema |
| root | root | mysql |


## Instalación

Acceder al directorio donde esta instalado magento.

```bash
$ cd ~/apps/magento2
```

Ejecutar los siguientes comandos para su instalación.

```bash
composer config repositories.srpago git https://git.srpago.com/plugins/magento.git && \
    composer require srpago/srpagopayments dev-master && \
    php bin/magento setup:upgrade && \
    php bin/magento setup:di:compile
```

Para acceder a la configuración de la extensión __Stores > Configuration__ y en el menú izquierdo __Sales > Payment Methods__.


## Actualización

Acceder al directorio donde esta instalado magento.

```bash
$ cd ~/apps/magento2
```

Ejecutar los siguientes comandos para su actualización.

```bash
composer update srpago/srpagopayments && \
    bin/magento setup:upgrade && \
    bin/magento setup:di:compile && \
    bin/magento cache:enable
```


## Webhook

La url para los webhooks es: __your_magento_store/srpago/webhook/listener__
